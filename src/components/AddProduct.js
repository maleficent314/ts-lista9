// import React, {Component} from "react";
// import './styles/AddProduct.css';
//
// class AddProduct extends Component{
//     constructor(props) {
//         super(props);
//         this.state = {product: ''};
//         this.handleUpdate = this.handleUpdate.bind(this);
//         this.addProduct = this.addProduct.bind(this);
//     }
//
//     handleUpdate(event) {
//         this.setState({product: event.target.value});
//     }
//
//     render() {
//         return (
//             <div className="AddProduct">
//                 <input type="text" onChange={this.handleUpdate}/>
//                 &nbsp;&nbsp;
//                 <button onClick={this.addProduct}>Add</button>
//             </div>
//         );
//     }
//
//     addProduct() {
//         this.props.addProduct(this.state.product);
//         this.setState({product: ''});
//     }
//
// }
//
// export default AddProduct;