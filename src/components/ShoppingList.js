import React from 'react';
import './styles/ShoppingList.css';
import ProductCell from "./ProductCell";
import {shoppingClasses} from '../shoppingClasses';

const ShoppingList = () => {
    const cells = shoppingClasses.map(shoppingClass => {
        return (
            <ProductCell
                shoppingClass={shoppingClass}
            />
        );
    });

    return (
        <section className="shopping-list">
            {cells}
        </section>
    )
}

export default ShoppingList;