import React from 'react';
import './styles/ProductCell.css';

const ProductCell = ({shoppingClass}) => {

    const {name, price, quantity} = shoppingClass;
    const styleMain = {
        fontSize: '17px',
        margin: '15px',
        border: '5px solid pink'
    };
    const styleName = {
        textAlign: 'center',
        fontSize: '23px',
        color: '#7400b8'
    };

    return (
        <section style={styleMain} className="product-cell">
            <div className='data-wrapper'>
                <h1 style={styleName} className='data-name'> {name}</h1>
                <p className="data-char">Price: {price} $</p>
                <p className="data-char">Quantity: {quantity}</p>
            </div>
        </section>
    )
};

export default ProductCell;

