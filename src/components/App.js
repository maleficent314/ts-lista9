import React, {Component} from 'react';
import './styles/App.css';
import ShoppingList from "./ShoppingList";
import AddProduct from "./AddProduct";

class App extends Component {
    render() {
        return (
            <div className="App">
                <ShoppingList/>
            </div>
        );
    }


}


export default App;