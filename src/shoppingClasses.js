export const shoppingClasses = [
    {
        name: "Bread",
        price: 5,
        quantity: 2

    },
    {
        name: "Milk",
        price: 6,
        quantity: 3

    },
    {
        name: "Eggs",
        price: 20,
        quantity: 15

    },
    {
        name: "Beer",
        price: 10,
        quantity: 5

    },
    {
        name: "Mustard",
        price: 12,
        quantity: 3

    },
    {
        name: "Apples",
        price: 14,
        quantity: 2

    },
    {
        name: "Orange",
        price: 12,
        quantity: 3

    },
    {
        name: "Bananas",
        price: 14,
        quantity: 2

    }
];